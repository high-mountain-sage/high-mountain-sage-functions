# Project Overview

These were the serverless functions used by the [JAMStack site](https://gitlab.com/high-mountain-sage/high-mountain-sage) for High Mountain Sage 🧔🏻‍♂️.

These functions provided features likes real-time shipping rates and inventory as well as mailing list related features i.e. signing up individuals to the mailing list, updating mailing list data based on
visitor activity to run automatic campaigns, and notifying customers when their orders were delivered.