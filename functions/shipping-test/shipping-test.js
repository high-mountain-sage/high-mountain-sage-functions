const axios = require('axios')
const EasyPost = require('@easypost/api')

const api = new EasyPost(process.env.EASYPOST_TEST)

async function verifySnipcart(token) {
  try {
    return await axios.get(
      `https://app.snipcart.com/api/requestvalidation/${token}`,
      {
        auth: {
          username: process.env.SNIPCART_TEST
        }
      })
  } catch {
    throw new Error('Could not validate Snipcart Request Token')
  }
}

exports.handler = async (event, context) => {
  // Validate request with Snipcart
  try {
    if (!event.headers['x-snipcart-requesttoken']){
        throw new Error('Missing Snipcart Request Token')
    }

    const snipcartToken = event.headers['x-snipcart-requesttoken']
    await verifySnipcart(snipcartToken)
  } catch(err) {
    console.log(err)

    return {
      statusCode: 403
    }
  }

  // Parse webhook content
  const shippingInfo = JSON.parse(event.body).content

  if (shippingInfo.totalWeight <= 0) {
    return {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ rates: [{ cost: 0, description: 'Digital Product' }] })
    }
  }

  const fromAddress = {
    company: 'High Mountain Sage',
    street1: '1071 Crescent Drive',
    city: 'Logan',
    state: 'UT',
    zip: '84341'
  }

  const parcel = {
    predfined_package: 'Parcel',
    weight: shippingInfo.totalWeight * 0.03527
  }

  const toAddress = {
    name: shippingInfo.shippingAddressName,
    company: shippingInfo.shippingAddressCompanyName,
    street1: shippingInfo.shippingAddressAddress1,
    street2: shippingInfo.shippingAddressAddress2,
    city: shippingInfo.shippingAddressCity,
    state: shippingInfo.shippingAddressProvince,
    zip: shippingInfo.shippingAddressPostalCode,
    country: shippingInfo.shippingAddressCountry,
    verify: ['delivery', 'zip4']
  }

  const rates = new api.Shipment({
    to_address: toAddress,
    from_address: fromAddress,
    parcel
  })

  const snipcartRates = []
  try {
    easypost = await rates.save()

    easypost.rates.forEach((rate) => {
      if (rate.service === 'First' || rate.service === 'Priority') {
        snipcartRates.push({
          cost: parseFloat(rate.rate),
          description: `${ rate.carrier } - ${ rate.service === 'First' ? 'First Class' : 'Priority Mail' }`
        })
      } else if (rate.service === 'FirstClassPackageInternationalService' || rate.service === 'PriorityMailInternational') {
        snipcartRates.push({
          cost: parseFloat(rate.rate),
          description: `${ rate.carrier } - ${ rate.service === 'FirstClassPackageInternationalService' ? 'First Class (International)' : 'Priorty Mail (International)' }`
        })
      }
    })

    // Cache Valley Delivery
    if (/^84(302|319|321|322|326|332|339|341)-/.test(easypost.buyer_address.zip)) {
      snipcartRates.push({
        cost: 1,
        description: 'Cache Valley Delivery: Delivery time scheduled through email'
      })
    }

    // Cache Valley Pick-Up
    if (/^84(302|318|319|321|322|326|332|335|339|341)-/.test(easypost.buyer_address.zip)) {
      snipcartRates.push({
          cost: 0,
          description: 'Cache Valley Pick-Up: Appointment scheduled through email'
      })
    }

    // VIP Shipping
    if (shippingInfo.total >= 50) {
      snipcartRates.push({
        cost: 0,
        description: 'VIP Shipping'
      })
    }

    snipcartRates.sort((a, b) => a.cost - b.cost)
  } catch(err) {
    console.log(err)
    return {
      statusCode: 200,
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        errors: [{
          key: err.error.error.error.errors[0].code,
          message: err.error.error.errors[0].message
        }]
      })
    }
  }

  return {
    statusCode: 200,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ rates: snipcartRates })
  }
}