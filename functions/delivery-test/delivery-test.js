const axios = require('axios').default
const EasyPost = require('@easypost/api')

const easyAPI = new EasyPost(process.env.EASYPOST_TEST)

exports.handler = async (event, context) => {
  const body = JSON.parse(event.body)
  const easyEvent = body.description
  const easyStatus = body.result.status
  const easyShipment = body.result.shipment_id

  if (easyEvent === 'tracker.updated' && easyStatus === 'delivered') {
    await easyAPI.Shipment.retrieve(easyShipment)
      .then(async (response) => {
        const orderToken = response.reference
        if (orderToken) {
          await axios.put(
            `https://app.snipcart.com/api/orders/${orderToken}`,
            {
              status: 'Delivered',
            },
            {
              auth: {
                username: process.env.SNIPCART_TEST
              }
            })
            .then(async () => {
              await axios.post(
                `https://app.snipcart.com/api/orders/${orderToken}/notifications`,
                {
                  type: 'Comment',
                  deliveryMethod: 'Email',
                  message: 'Customer has been notified that their order has been delivered'
                },
                {
                  auth: {
                    username: process.env.SNIPCART_TEST
                  }
                })
                .then((response) => {
                  console.log(response.data)
                })
                .catch((error) => {
                  console.log(error.status, error.statusText)
                })
            })
            .catch((error) => {
              console.log(error.status, 'Order Token not found')
            })
        } else {
          console.log('No order reference')
        }
      })
  }
}