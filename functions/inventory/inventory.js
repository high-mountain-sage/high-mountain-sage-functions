const axios = require('axios')

const api = 'https://app.snipcart.com/api/products?limit=50&offset=0'

exports.handler = async (event, context) => {
    return await axios.get(
        api,
        {
            auth: {
                username: process.env.INVENTORY_LIVE
            },
        }
    )
    .then(response => {
        let inventory = response.data.items.map(item => {
            return {
                id: item.userDefinedId,
                name: item.name,
                stock: item.stock !== undefined ? item.stock : -1
            }
        })

        return {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'inventory': inventory
            })
        }
    })
    .catch(error => {
        return {
            statusCode: error.statusCode || 500,
            body: JSON.stringify({
                error: error.message
            })
        }
    })
}