exports.handler = (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify({
            message: 'Hi there',
            context: context,
            event: event,
        }),
    }

    return callback(null, response)
}