const axios = require('axios')

const emailAPI = 'https://api.mailerlite.com/api/v2/groups/105235130/subscribers'

async function addSubscriber(subscriberInfo) {
  return await axios.post(
    emailAPI,
    {...subscriberInfo},
    {
      headers: {
        'Content-Type': 'application/json',
        'X-MailerLite-ApiKey': process.env.MAILER_LITE_KEY
      }
    })
}

async function getCustomerInfo(customerId) {
  const response = await axios.get(
    `https://app.snipcart.com/api/customers/${customerId}`,
    {
      auth: {
        username: process.env.SNIPCART_LIVE
      }
    })
  return response.data
}

async function getSubscriber(email) {
  const response = await axios.get(
    `https://api.mailerlite.com/api/v2/subscribers/${email}`,
    {
      headers: {
        'Content-Type': 'application/json',
        'X-MailerLite-ApiKey': process.env.MAILER_LITE_KEY
      }
    })
  return response.data
}

async function verifySnipcart(token) {
  try {
    return await axios.get(
      `https://app.snipcart.com/api/requestvalidation/${token}`,
      {
        auth: {
          username: process.env.SNIPCART_LIVE
        }
      })
  } catch {
    throw new Error('Could not validate Snipcart Request Token')
  }
}

exports.handler = async (event, context) => {
  // Validate request with Snipcart
  try {
    if (!event.headers['x-snipcart-requesttoken']) {
      throw new Error('Missing Snipcart Request Token');
    }

    const snipcartToken = event.headers['x-snipcart-requesttoken']
    await verifySnipcart(snipcartToken)
  } catch(error) {
    console.log(error)

    return {
      statusCode: 403
    }
  }

  // Parse webhook content
  const body = JSON.parse(event.body)
  const eventName = body.eventName
  const eventInfo = body.content

  if (eventName === 'order.completed' && eventInfo.customFields) {
    const newsletter = eventInfo.customFields.find((field) => field.name === 'newsletter')

    const customer = eventInfo.user
    const email = customer.email
    const name = customer.billingAddressName

    let currentSubscriber
    try {
      currentSubscriber = await getSubscriber(email)
    } catch {
      console.log('Customer is not a mailing list subscriber')
    }

    if (
      newsletter.value === 'true'
      || (currentSubscriber && currentSubscriber.type === 'active')
    ) {
      const recentPurchaseName = eventInfo.items[0].name
      const recentPurchaseURL = eventInfo.items[0].url

      const customerInfo = await getCustomerInfo(customer.id)
      const ordersAmount = customerInfo.statistics.ordersAmount
      const ordersCount = customerInfo.statistics.ordersCount

      const subscriberInfo = {
        email,
        name,
        fields: {
          form: 'checkout',
          orders_amount: ordersAmount,
          orders_count: ordersCount,
          recent_purchase_name: recentPurchaseName,
          recent_purchase_url: recentPurchaseURL
        }
      }

      try {
        await addSubscriber(subscriberInfo)
      } catch({ response }) {
        console.log('Bad Request')

        return {
          statusCode: response.status,
        }
      }
    }
  }

  return {
    statusCode: 200
  }
}