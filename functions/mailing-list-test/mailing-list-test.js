const axios = require('axios')

const emailAPI = 'https://api.mailerlite.com/api/v2/groups/105235130/subscribers'

exports.handler = async (event, context) => {
    const body = JSON.parse(event.body)
    const bot = body.bot
    const emailAddress = body.email

    // Trap bots
    if (bot) {
        return {
            statusCode: 403,
            body: JSON.stringify({
                error: 'Bad Robot'
            })
        }
    }

    // Verify email
    try {
        const verification = await axios.get(
            `https://verifier.meetchopra.com/verify/${emailAddress}`,
            {
                params: {
                    token: process.env.E_VERIFIER_KEY,
                }
            }
        )

        if (!verification.data.status) {
            console.log(verification.data.error.message)
            return {
                statusCode: 400,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    error: 'We couldn\'t process your e-mail address. Please try a different address.'
                })
            }
        }
    } catch(error) {
        return {
            statusCode: error.statusCode || 500,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                error: 'Please email us at admin@highmountainsage.com'
            })
        }
    }

    // Add email to mailing list
    try {
        const response = await axios({
            method: 'post',
            url: emailAPI,
            data: {
                email: emailAddress,
                fields: {
                    form: 'signUp'
                }
            },
            headers: {
                'Content-Type': 'application/json',
                'X-MailerLite-ApiKey': process.env.MAILER_LITE_KEY
            }
        })

        return {
            statusCode: 200,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                data: response.data
            })
        }
    } catch(error) {
        return {
            statusCode: error.statusCode || 500,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                error: 'Please email us at admin@highmountainsage.com'
            })
        }
    }
}